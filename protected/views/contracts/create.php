<?php
/** @var $this ContractsController */
/** @var $model Contract */
?>

<h1 class="page-header">Create Contract</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>