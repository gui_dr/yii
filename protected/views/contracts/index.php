<?php
/** @var $this ContractsController */
/** @var $model Contract */
?>

<h1 class="page-header">Manage Contracts</h1>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-3">
                <a href="<?php echo $this->createUrl('//contracts/create'); ?>" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> Create
                </a>
            </div>
            <div class="col-sm-9 text-right">
                <div class="btn-group">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <?php if (isset($_GET['company'])) : ?>
                                <?php echo Company::model()->findByPk($_GET['company'])->name; ?>
                            <?php else : ?>
                                All companies
                            <?php endif; ?>
                            &nbsp;<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach (Company::model()->findAll() as $company) : ?>
                                <li>
                                    <a href="<?php echo $this->createUrl('//contracts') . '?company=' . $company->id . (isset($_GET['user']) ? '&user=' . $_GET['user'] : ''); ?>">
                                        <?php if (isset($_GET['company']) && $_GET['company'] == $company->id) : ?>
                                            <span class="glyphicon glyphicon-ok"></span>&nbsp;
                                        <?php endif; ?>
                                        <?php echo $company->name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo $this->createUrl('//contracts') . (isset($_GET['user']) ? '?user=' . $_GET['user'] : ''); ?>">
                                    <?php if (!isset($_GET['company'])) : ?>
                                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                                    <?php endif; ?>
                                    All companies
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <?php if (isset($_GET['user'])) : ?>
                                <?php echo User::model()->findByPk($_GET['user'])->username; ?>
                            <?php else : ?>
                                All users
                            <?php endif; ?>
                            &nbsp;<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach (User::model()->findAll() as $user) : ?>
                                <li>
                                    <a href="<?php echo $this->createUrl('//contracts') . '?user=' . $user->id . (isset($_GET['company']) ? '&company=' . $_GET['company'] : ''); ?>">
                                        <?php if (isset($_GET['user']) && $_GET['user'] == $user->id) : ?>
                                            <span class="glyphicon glyphicon-ok"></span>&nbsp;
                                        <?php endif; ?>
                                        <?php echo $user->username; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo $this->createUrl('//contracts') . (isset($_GET['company']) ? '?company=' . $_GET['company'] : ''); ?>">
                                    <?php if (!isset($_GET['user'])) : ?>
                                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                                    <?php endif; ?>
                                    All users
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th class="col-sm-1"></th>
                <th class="col-sm-4">Summary</th>
                <?php if (!isset($_GET['company'])) : ?>
                    <th class="col-sm-2">Company</th>
                <?php endif; ?>
                <?php if (!isset($_GET['user'])) : ?>
                    <th class="col-sm-2">Responsible</th>
                <?php endif; ?>
                <th class="col-sm-1 text-right">Cost</th>
                <th class="col-sm-2 text-right">Due Date</th>
            </tr>
        </thead>
        <tbody>
            <?php /** @var $contract Contract */ ?>
            <?php foreach ($model->search()->getData() as $contract) : ?>
                <tr<?php if (!$contract->active) : ?> class="danger"<?php endif; ?>>
                    <td class="text-center">
                        <a href="<?php echo $this->createUrl('//contracts/update', array('id' => $contract->id)); ?>" class="btn btn-default btn-sm" title="View">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                    <td>
                        <strong><?php echo $contract->summary; ?></strong><br>
                        <?php echo $contract->client->name; ?>
                    </td>
                    <?php if (!isset($_GET['company'])) : ?>
                        <td><?php echo $contract->company->name; ?></td>
                    <?php endif; ?>
                    <?php if (!isset($_GET['user'])) : ?>
                        <td><?php echo $contract->responsible->username; ?></td>
                    <?php endif; ?>
                    <td class="text-right">£<?php echo number_format($contract->cost ?: 0, 2); ?></td>
                    <td class="text-right"><?php echo date('d M Y', strtotime($contract->due_date)); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="panel-footer">
        <fieldset>
            <legend>Legend</legend>
            <span class="label label-danger">Inactive contract</span>
        </fieldset>
    </div>
</div>