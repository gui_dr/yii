<?php
/** @var $this ContractsController */
/** @var $model Contract */

/** @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'contract-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'role' => 'form',
        'class' => 'form-horizontal',
    ),
));
?>

<?php $this->renderPartial('../layouts/_messages', array('model' => $model)); ?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group required<?php if ($model->hasErrors('summary')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'summary', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->textField($model, 'summary', array('maxlength' => 255, 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('summary')) : ?><p class="help-block"><?php echo $form->error($model, 'summary'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group required<?php if ($model->hasErrors('client_id')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'client_id', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->dropDownList($model, 'client_id', CHtml::listData(Client::model()->findAll(), 'id', 'name'), array('empty' => 'Select a client...', 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('client_id')) : ?><p class="help-block"><?php echo $form->error($model, 'client_id'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group required<?php if ($model->hasErrors('company_id')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'company_id', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(), 'id', 'name'), array('empty' => 'Select a company...', 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('company_id')) : ?><p class="help-block"><?php echo $form->error($model, 'company_id'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group required<?php if ($model->hasErrors('responsible_id')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'responsible_id', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->dropDownList($model, 'responsible_id', CHtml::listData(User::model()->findAll(), 'id', 'username'), array('empty' => 'Select a responsible...', 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('responsible_id')) : ?><p class="help-block"><?php echo $form->error($model, 'responsible_id'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'start_date', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4<?php if ($model->hasErrors('start_date')) : ?> has-error<?php endif; ?>">
                <div class="input-group">
                    <?php echo $form->textField($model, 'start_date', array('maxlength' => 50, 'class' => 'form-control')); ?>
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                </div>
                <?php if ($model->hasErrors('start_date')) : ?><p class="help-block"><?php echo $form->error($model, 'start_date'); ?></p><?php endif; ?>
            </div>

            <?php echo $form->labelEx($model, 'due_date', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4<?php if ($model->hasErrors('due_date')) : ?> has-error<?php endif; ?>">
                <div class="input-group">
                    <?php echo $form->textField($model, 'due_date', array('maxlength' => 50, 'class' => 'form-control')); ?>
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                </div>
                <?php if ($model->hasErrors('due_date')) : ?><p class="help-block"><?php echo $form->error($model, 'due_date'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'cost', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4<?php if ($model->hasErrors('cost')) : ?> has-error<?php endif; ?>">
                <div class="input-group">
                    <div class="input-group-addon">£</div>
                    <?php echo $form->textField($model, 'cost', array('maxlength' => 50, 'class' => 'form-control')); ?>
                </div>
                <?php if ($model->hasErrors('cost')) : ?><p class="help-block"><?php echo $form->error($model, 'cost'); ?></p><?php endif; ?>
            </div>

            <?php echo $form->labelEx($model, 'finish_date', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4<?php if ($model->hasErrors('finish_date')) : ?> has-error<?php endif; ?>">
                <div class="input-group">
                    <?php echo $form->textField($model, 'finish_date', array('maxlength' => 50, 'class' => 'form-control')); ?>
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                </div>
                <?php if ($model->hasErrors('finish_date')) : ?><p class="help-block"><?php echo $form->error($model, 'finish_date'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'description', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->textArea($model, 'description', array('rows' => 6, 'class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <?php echo $form->checkBox($model, 'active', array('checked' => 'checked')); ?>
                <?php echo $form->labelEx($model, 'active'); ?>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-default">Cancel</a>

        <div class="pull-right">
            <a href="<?php echo $this->createUrl('//contracts/delete', array('id' => $model->id)); ?>" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span> Delete
            </a>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>