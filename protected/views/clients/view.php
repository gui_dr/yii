<?php
/* @var $this ClientsController */
/* @var $model Client */
?>

<h1 class="page-header">Client Details</h1>

<div class="panel panel-default">
    <div class="panel-body">
        <fieldset>
            <legend>Name: <?php echo $model->name; ?></legend>
            <dl>
                <dt>Notes:</dt>
                <dd><?php echo nl2br($model->notes); ?></dd>
            </dl>
        </fieldset>
    </div>

    <div class="panel-footer">
        <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-default">Back</a>

        <div class="pull-right">
            <div class="btn-group">
                <a href="<?php echo $this->createUrl('//clients/update', array('id' => $model->id)); ?>" class="btn btn-default">
                    <span class="glyphicon glyphicon-pencil"></span> Update
                </a>
                <a href="<?php echo $this->createUrl('//clients/delete', array('id' => $model->id)); ?>" class="btn btn-danger">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </a>
            </div>
        </div>
    </div>
</div>