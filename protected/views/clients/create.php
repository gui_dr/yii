<?php
/** @var $this ClientsController */
/** @var $model Client */
?>

<h1 class="page-header">Create Client</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>