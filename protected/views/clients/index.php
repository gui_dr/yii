<?php
/* @var $this ClientsController */
/* @var $model Client */
?>

<h1 class="page-header">Manage Clients</h1>

<?php $this->renderPartial('../layouts/_messages', array('model' => $model)); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6">
                <a href="<?php echo $this->createUrl('//clients/create'); ?>" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> Create
                </a>
            </div>

            <div class="col-md-6">
                <?php $this->renderPartial('../layouts/_quicksearch', array('model' => $model)); ?>
            </div>
        </div>
    </div>

    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th class="col-md-10">Name</th>
                <th class="col-md-2"></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($dataProvider->getData() as $client) : ?>
            <tr>
                <td><?php echo $client->name; ?></td>
                <td class="text-right">
                    <div class="btn-group">
                        <a href="<?php echo $this->createUrl('//clients/view', array('id' => $client->id)); ?>" class="btn btn-default btn-sm" title="View">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                        <a href="<?php echo $this->createUrl('//clients/update', array('id' => $client->id)); ?>" class="btn btn-default btn-sm" title="Update">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="<?php echo $this->createUrl('//clients/delete', array('id' => $client->id)); ?>" class="btn btn-danger btn-sm" title="Delete">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="panel-footer">
        <div class="text-right">
            <?php $this->renderPartial('../layouts/_pagination', array('pagination' => $dataProvider->getPagination())); ?>
        </div>
    </div>
</div>