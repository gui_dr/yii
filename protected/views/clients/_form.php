<?php
/* @var $this ClientsController */
/* @var $model Client */

/* @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'client-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'role' => 'form',
        'class' => 'form-horizontal',
    ),
));
?>

<?php $this->renderPartial('../layouts/_messages', array('model' => $model)); ?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group required<?php if ($model->hasErrors('name')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'name', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('name')) : ?><p class="help-block"><?php echo $form->error($model, 'name'); ?></p><?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'notes', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->textArea($model, 'notes', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-default">Cancel</a>
    </div>
</div>

<?php $this->endWidget(); ?>