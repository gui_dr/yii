<?php
/** @var $this ClientsController */
/** @var $model Client */
?>

<h1 class="page-header">Update Client #<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>