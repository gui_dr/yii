<?php
/** @var $this UsersController */
/** @var $model User */

/* @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'delete-form',
    'action' => $this->createUrl($this->route, array('id' => $model->id)),
    'htmlOptions' => array(
        'role' => 'form',
    ),
));
?>
<h1 class="page-header">Confirmation</h1>

<div class="panel panel-default">
    <div class="panel-body">
        <p><strong>Are you sure you want to delete the user "<?php echo $model->username; ?>"?</strong></p>
        <p>This action cannot be undone.</p>
    </div>

    <div class="panel-footer">
        <?php echo CHtml::submitButton('Confirm', array('class' => 'btn btn-primary')); ?>&nbsp;
        <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-default">Cancel</a>
    </div>
<?php $this->endWidget(); ?>