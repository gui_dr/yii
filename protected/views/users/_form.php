<?php
/* @var $this UsersController */
/* @var $model User */

/* @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'role' => 'form',
        'class' => 'form-horizontal',
    ),
));
?>

<?php $this->renderPartial('../layouts/_messages', array('model' => $model)); ?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group required<?php if ($model->hasErrors('username')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'username', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->textField($model, 'username', array('size' => 60, 'maxlength' => 128, 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('username')) : ?><p class="help-block"><?php echo $form->error($model, 'username'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group required<?php if ($model->hasErrors('password')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'password', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 128, 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('password')) : ?><p class="help-block"><?php echo $form->error($model, 'password'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="form-group required<?php if ($model->hasErrors('email')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'email', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128, 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('email')) : ?><p class="help-block"><?php echo $form->error($model, 'email'); ?></p><?php endif; ?>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-default">Cancel</a>
    </div>
</div>

<?php $this->endWidget(); ?>