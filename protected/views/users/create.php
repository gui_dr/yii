<?php
/** @var $this UsersController */
/** @var $model User */
?>
<h1 class="page-header">Create user</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>