<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */
?>

<h1 class="page-header">Manage Users</h1>

<?php $this->renderPartial('../layouts/_messages', array('model' => $model)); ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6">
                <a href="<?php echo $this->createUrl('//users/create'); ?>" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> Create
                </a>
            </div>

            <div class="col-md-6">
                <?php $this->renderPartial('../layouts/_quicksearch', array('model' => $model)); ?>
            </div>
        </div>
    </div>

    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th class="col-md-5">Name</th>
                <th class="col-md-5">Email</th>
                <th class="col-md-2"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dataProvider->getData() as $user) : ?>
                <tr>
                    <td><?php echo $user->username; ?></td>
                    <td><a href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a></td>
                    <td class="text-right">
                        <div class="btn-group">
                            <a href="<?php echo $this->createUrl('//users/update', array('id' => $user->id)); ?>" class="btn btn-default btn-sm" title="Update">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a href="<?php echo $this->createUrl('//users/delete', array('id' => $user->id)); ?>" class="btn btn-danger btn-sm" title="Delete">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="panel-footer text-right">
        <?php $this->renderPartial('../layouts/_pagination', array('pagination' => $dataProvider->getPagination())); ?>
    </div>
</div>