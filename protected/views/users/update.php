<?php
/* @var $this UsersController */
/* @var $model User */
?>

<h1 class="page-header">Update User #<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>