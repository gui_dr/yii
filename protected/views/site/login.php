<?php
/** @var $this SiteController */
/** @var $model LoginForm */

$this->pageTitle = Yii::app()->name . ' - Login';

$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->request->baseUrl . '/assets/css/login.styles.css');

/** @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
));
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Sign in to continue</h2>
    </div>
    <div class="panel-body">
        <?php if ($model->hasErrors()) : ?>
            <div class="alert alert-danger" role="alert">
                <?php foreach ($model->getErrors() as $error) : ?>
                    <p><?php echo $error[0]; ?></p>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <div class="form-group<?php if ($model->hasErrors('username')) : ?> has-error<?php endif; ?>">
            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'Email address', 'autofocus' => 'autofocus')); ?>
            </div>
        </div>

        <div class="form-group<?php if ($model->hasErrors('password')) : ?> has-error<?php endif; ?>">
            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
            </div>
        </div>

        <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-lg btn-success btn-block')); ?>
    </div>
</div>

<?php $this->endWidget(); ?>