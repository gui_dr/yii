<?php
/* @var $this CompaniesController */
/* @var $model Company */

/* @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'company-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'role' => 'form',
        'class' => 'form-horizontal',
    ),
));
?>

<?php $this->renderPartial('../layouts/_messages', array('model' => $model)); ?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group required<?php if ($model->hasErrors('name')) : ?> has-error<?php endif; ?>">
            <?php echo $form->labelEx($model, 'name', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php if ($model->hasErrors('name')) : ?><p class="help-block"><?php echo $form->error($model, 'name'); ?></p><?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">Location</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'country', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?php echo $form->textField($model, 'country', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'city', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-6">
                                <?php echo $form->textField($model, 'city', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                            </div>

                            <?php echo $form->labelEx($model, 'state', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-3">
                                <?php echo $form->textField($model, 'state', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'address', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?php echo $form->textField($model, 'address', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'postcode', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?php echo $form->textField($model, 'postcode', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">Contact</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'telephone', array('class' => 'col-sm-3 control-label')); ?>
                            <div class="col-sm-9">
                                <?php echo $form->textField($model, 'telephone', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
        <a href="<?php echo Yii::app()->request->urlReferrer; ?>" class="btn btn-default">Cancel</a>
    </div>
</div>

<?php $this->endWidget(); ?>