<?php
/** @var $this CompaniesController */
/** @var $model Company */
?>

<h1 class="page-header">Company Details</h1>

<div class="panel panel-default">
    <div class="panel-body">
        <fieldset>
            <legend>Name: <?php echo $model->name; ?></legend>
            <dl>
                <dt>Telephone</dt>
                <dd><?php echo $model->telephone; ?></dd>

                <dt>Location</dt>
                <dd><?php echo $model->getLocation(); ?></dd>

                <dt>Address</dt>
                <dd><?php echo $model->address; ?></dd>

                <dt>Postcode</dt>
                <dd><?php echo $model->postcode; ?></dd>
            </dl>
        </fieldset>
    </div>

    <div class="panel-footer">
        <a href="<?php echo $this->getRequest()->urlReferrer; ?>" class="btn btn-default">Back</a>

        <div class="pull-right">
            <div class="btn-group">
                <a href="<?php echo $this->createUrl('//companies/update', array('id' => $model->id)); ?>" class="btn btn-default">
                    <span class="glyphicon glyphicon-pencil"></span> Update
                </a>
                <a href="<?php echo $this->createUrl('//companies/delete', array('id' => $model->id)); ?>" class="btn btn-danger">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </a>
            </div>
        </div>
    </div>
</div>