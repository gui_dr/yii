<?php
/** @var $this CompaniesController */
/** @var $model Company */
?>

<h1 class="page-header">Create Company</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>