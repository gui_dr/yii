<?php
/** @var $this CompaniesController */
/** @var $model Company */
?>

<h1 class="page-header">Update Company #<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>