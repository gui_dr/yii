<?php
/** @var $pagination CPagination */
$urlParams = array();

if (isset($_GET['q']) && $_GET['q'] != '') {
    // append the current search query string to the params
    $urlParams['q'] = $_GET['q'];
}
?>
<ul class="pagination">
    <li<?php if ($pagination->currentPage == 0) : ?> class="disabled"<?php endif; ?>>
        <a href="<?php echo $pagination->currentPage == 0 ? '#' : Yii::app()->createUrl($this->route, array_merge(array('page' => $pagination->currentPage), $urlParams)); ?>">
            &laquo;
        </a>
    </li>

    <?php for ($i = 0; $i < $pagination->pageCount; $i++) : ?>
        <li<?php if ($i == $pagination->currentPage) : ?> class="active"<?php endif; ?>>
            <a href="<?php echo $i == $pagination->currentPage ? '#' : Yii::app()->createUrl($this->route, array_merge(array('page' => $i + 1), $urlParams)); ?>">
                <?php echo $i + 1; ?>
            </a>
        </li>
    <?php endfor; ?>

    <li<?php if ($pagination->currentPage == $pagination->pageCount - 1) : ?> class="disabled"<?php endif; ?>>
        <a href="<?php echo $pagination->currentPage == $pagination->pageCount - 1 ? '#' : Yii::app()->createUrl($this->route, array_merge(array('page' => $pagination->currentPage + 2), $urlParams)); ?>">
            &raquo;
        </a>
    </li>
</ul>