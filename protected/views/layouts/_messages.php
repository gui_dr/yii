<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if ((isset($model) && $model->hasErrors()) || Yii::app()->user->hasFlash('error')) : ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
        <?php if (Yii::app()->user->hasFlash('error')) : ?>
            <?php echo Yii::app()->user->getFlash('error'); ?>
        <?php else : ?>
            <strong>Validation errors: </strong>The form contains errors and could not be submitted.
        <?php endif; ?>
    </div>
<?php endif; ?>