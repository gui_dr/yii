<?php /** @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <style type="text/css">
        .table thead > tr > th > a,
        .table thead > tr > th > a:hover {
            color: #000000;
            text-decoration: none;
        }
        .table tbody > tr > td {
            vertical-align: middle !important;
        }
    </style>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/admin.styles.css">

    <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/html5shiv.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li><a href="/"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo $this->createUrl('//site/logout'); ?>"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <fieldset>
                    <legend>Main</legend>
                    <ul class="nav nav-sidebar">
                        <li>
                            <a href="<?php echo $this->createUrl('//contracts') . '?user=' . $this->getUser()->getId(); ?>"><span class="glyphicon glyphicon-gbp"></span> Contracts</a>
                        </li>
                    </ul>
                </fieldset>
                <fieldset>
                    <legend>General</legend>
                    <ul class="nav nav-sidebar">
                        <li>
                            <a href="<?php echo $this->createUrl('//users'); ?>"><span class="glyphicon glyphicon-user"></span> Users</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createUrl('//clients'); ?>"><span class="glyphicon glyphicon-briefcase"></span> Clients</a>
                        </li>
                        <li>
                            <a href="<?php echo $this->createUrl('//companies'); ?>"><span class="glyphicon glyphicon-globe"></span> Companies</a>
                        </li>
                    </ul>
                </fieldset>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <?php echo $content; ?>
            </div>
        </div>
    </div>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>