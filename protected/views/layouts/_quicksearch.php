<?php
/* @var $form CActiveForm */
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'htmlOptions' => array(
        'role' => 'search',
    ),
));
?>

<div class="input-group">
    <input type="text" name="q" class="form-control" placeholder="Search..." value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''; ?>">
    <div class="input-group-btn">
        <?php if (isset($_GET['q']) && $_GET['q'] != '') : ?>
            <a href="<?php echo Yii::app()->createUrl($this->route); ?>" class="btn btn-default" title="Clear">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
        <?php endif; ?>

        <button type="submit" class="btn btn-default" title="Search">
            <span class="glyphicon glyphicon-search"></span>
        </button>
    </div>
</div>

<?php $this->endWidget(); ?>