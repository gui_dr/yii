<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 * @property integer $id
 * @property string $name
 * @property string $notes
 */
class Client extends CActiveRecord
{
    /**
     * @return string
     */
    public function tableName()
    {
        return 'clients';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 100),
            array('notes', 'safe'),

            // The following rule is used by search().
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array
     */
    public function relations()
    {
        return array(
            'contracts' => array(self::HAS_MANY, 'Contract', 'contract_id'),
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'notes' => 'Notes',
        );
    }

    /**
     * @param string $q
     * @return CActiveDataProvider
     */
    public function search($q = '')
    {
        $criteria = new CDbCriteria();

        if ($q != '') {
            $criteria->compare('name', $q, true);
        }

        $criteria->order = 'UPPER(name)';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $className
     * @return Client
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
