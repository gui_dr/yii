<?php

/**
 * This is the model class for table "companies".
 *
 * The followings are the available columns in table 'companies':
 * @property integer $id
 * @property string $name
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $address
 * @property string $postcode
 * @property string $telephone
 */
class Company extends CActiveRecord
{
    /**
     * @return string
     */
    public function tableName()
    {
        return 'companies';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name, country, state, city, telephone', 'length', 'max' => 100),
            array('address', 'length', 'max' => 255),
            array('postcode', 'length', 'max' => 50),

            // The following rule is used by search().
            array('id, name, country, state, city, address, postcode, telephone', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array
     */
    public function relations()
    {
        return array(
            'contracts' => array(self::HAS_MANY, 'Contract', 'company_id'),
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'address' => 'Address',
            'postcode' => 'Postcode',
            'telephone' => 'Telephone',
        );
    }

    /**
     * @param string $q
     * @return CActiveDataProvider
     */
    public function search($q = '')
    {
        $criteria = new CDbCriteria();

        if ($q != '') {
            $criteria->compare('name', $q, true);
        }

        $criteria->order = 'UPPER(name)';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $className
     * @return Company
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        $content = array();

        if (!empty($this->country)) {
            $content[] = $this->country;

            if (!empty($this->state)) {
                $content[] = $this->state;
            }
        }

        if (!empty($this->city)) {
            $content[] = $this->city;
        }

        return empty($content) ? '' : implode(', ', $content);
    }
}
