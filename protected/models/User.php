<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 */
class User extends CActiveRecord
{
    /**
     * @return string
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('username, password, email', 'required'),
            array('username, password, email', 'length', 'max' => 128),

            // The following rule is used by search().
            array('id, username, email', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array
     */
    public function relations()
    {
        return array(
            'contracts' => array(self::HAS_MANY, 'Contract', 'responsible_id'),
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
        );
    }

    /**
     * @param string $q
     * @return CActiveDataProvider
     */
    public function search($q = '')
    {
        $criteria = new CDbCriteria();

        if ($q !== '') {
            $criteria->compare('username', $q, true, 'OR');
            $criteria->compare('email', $q, true, 'OR');
        }

        $criteria->order = 'UPPER(username)';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $className
     * @return User
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
