<?php

/**
 * This is the model class for table "contracts".
 *
 * The followings are the available columns in table 'contracts':
 * @property integer $id
 * @property integer $client_id
 * @property integer $company_id
 * @property string $summary
 * @property string $created_date
 * @property integer $responsible_id
 * @property string $start_date
 * @property string $due_date
 * @property string $finish_date
 * @property double $cost
 * @property string $description
 * @property boolean $active
 *
 * The followings are the available model relations:
 * @property User $responsible
 * @property Company $company
 * @property Client $client
 */
class Contract extends CActiveRecord
{
    /**
     * @return string
     */
    public function tableName()
    {
        return 'contracts';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('client_id, company_id, summary, created_date, responsible_id', 'required'),
            array('client_id, company_id, responsible_id', 'numerical', 'integerOnly' => true),
            array('created_date, start_date, due_date, finish_date', 'type', 'type' => 'date', 'message' => '{attribute} is not a valid date.', 'dateFormat' => 'yyyy-MM-dd'),
            array('cost', 'numerical'),
            array('summary', 'length', 'max' => 255),
            array('start_date, due_date, finish_date, description', 'safe'),

            // The following rule is used by search().
            array('id, client_id, company_id, summary, created_date, responsible_id, start_date, due_date, finish_date, cost, description, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array
     */
    public function relations()
    {
        return array(
            'responsible' => array(self::BELONGS_TO, 'User', 'responsible_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'client_id' => 'Client',
            'company_id' => 'Company',
            'summary' => 'Summary',
            'created_date' => 'Created Date',
            'responsible_id' => 'Responsible',
            'start_date' => 'Start Date',
            'due_date' => 'Due Date',
            'finish_date' => 'Finish Date',
            'cost' => 'Cost',
            'description' => 'Description',
            'active' => 'Active',
        );
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('company_id', $this->company_id);
        $criteria->compare('responsible_id', $this->responsible_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $className
     * @return Contract
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
