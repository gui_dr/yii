<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    const FLASH_SUCCESS = 'success';
    const FLASH_ERROR = 'error';

    /**
     * @var string
     */
    public $layout = '//layouts/main';

    /**
     * @return UserIdentity
     */
    protected function getUser()
    {
        return Yii::app()->user;
    }

    /**
     * @return CHttpRequest
     */
    protected function getRequest()
    {
        return Yii::app()->request;
    }

    /**
     * @param string $type
     * @param string $message
     */
    protected function setFlash($type, $message)
    {
        $this->getUser()->setFlash($type, $message);
    }
}
