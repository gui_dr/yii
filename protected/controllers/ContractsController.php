<?php

class ContractsController extends Controller
{
    /**
     * @var string
     */
    public $layout = '//layouts/admin';

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $model = new Contract();

        if (isset($_GET['user'])) {
            $model->responsible_id = (int) $_GET['user'];
        }

        if (isset($_GET['company'])) {
            $model->company_id = (int) $_GET['company'];
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $model = new Contract();

        if (isset($_POST['Contract'])) {
            $model->attributes = $_POST['Contract'];
            $model->created_date = date('Y-m-d');
            $model->active = (bool) $_POST['Contract']['active'];

            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, 'Contract created');
                $this->redirect($this->createUrl('//contracts'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Contract'])) {
            $model->attributes = $_POST['Contract'];
            $model->active = (bool) $_POST['Contract']['active'];

            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, 'Contract updated');
                $this->redirect($this->createUrl('//contracts'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        if ($this->getRequest()->isPostRequest) {
            if ($model->delete()) {
                $this->setFlash(self::FLASH_SUCCESS, 'Contract deleted');
            }

            $this->redirect($this->createUrl('//contracts'));
        } else {
            $this->render('delete', array(
                'model' => $model,
            ));
        }
    }

    /**
     * @param integer $id
     * @return Contract
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Contract::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model->cost = number_format($model->cost, 2, '.', '');

        return $model;
    }
}
