<?php

class UsersController extends Controller
{
    /**
     * @var string
     */
    public $layout = '//layouts/admin';

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values

        $dataProvider = $model->search(isset($_GET['q']) ? $_GET['q'] : '');

        if (isset($_GET['page'])) {
            $dataProvider->pagination->currentPage = ((int) $_GET['page']) - 1;
        }

        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionCreate()
    {
        $model = new User();

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->password = md5($model->password);

            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('User %s created', $model->username));
                $this->redirect($this->createUrl('//users'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['User'])) {
            // verify if the password has been changed
            if ($_POST['User']['password'] === md5('PROTECTEDPASSWORDSTRING')) {
                $_POST['User']['password'] = $model->password;
            } else {
                $_POST['User']['password'] = md5($_POST['User']['password']);
            }

            $model->attributes = $_POST['User'];

            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('User %s updated', $model->username));
                $this->redirect($this->createUrl('//users'));
            }
        } else {
            // replace the password value for security
            $model->password = md5('PROTECTEDPASSWORDSTRING');
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        if ($this->getRequest()->isPostRequest) {
            if ($this->getUser()->getId() == $id) {
                $this->setFlash(self::FLASH_ERROR, 'You cannot remove yourself');
            } elseif ($model->delete()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('User %s deleted', $model->username));
            }

            $this->redirect($this->createUrl('//users'));
        } else {
            $this->render('delete', array(
                'model' => $model,
            ));
        }
    }

    /**
     * @param integer $id
     * @return User
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }
}
