<?php

class CompaniesController extends Controller
{
    /**
     * @var string
     */
    public $layout = '//layouts/admin';

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $model = new Company('search');
        $model->unsetAttributes();  // clear any default values

        $dataProvider = $model->search(isset($_GET['q']) ? $_GET['q'] : '');

        if (isset($_GET['page'])) {
            $dataProvider->pagination->currentPage = ((int) $_GET['page']) - 1;
        }

        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model = new Company();

        if (isset($_POST['Company'])) {
            $model->attributes = $_POST['Company'];
            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('Company %s created', $model->name));
                $this->redirect($this->createUrl('//companies'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Company'])) {
            $model->attributes = $_POST['Company'];
            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('Client %s updated', $model->name));
                $this->redirect($this->createUrl('//companies'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        if ($this->getRequest()->isPostRequest) {
            if ($model->delete()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('Company %s deleted', $model->name));
            }

            $this->redirect($this->createUrl('//companies'));
        } else {
            $this->render('delete', array(
                'model' => $model,
            ));
        }
    }

    /**
     * @param integer $id
     * @return Company
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Company::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }
}
