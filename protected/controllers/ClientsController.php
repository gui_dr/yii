<?php

class ClientsController extends Controller
{
    /**
     * @var string
     */
    public $layout = '//layouts/admin';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $model = new Client('search');
        $model->unsetAttributes();  // clear any default values

        $dataProvider = $model->search(isset($_GET['q']) ? $_GET['q'] : '');

        if (isset($_GET['page'])) {
            $dataProvider->pagination->currentPage = ((int) $_GET['page']) - 1;
        }

        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model = new Client();

        if (isset($_POST['Client'])) {
            $model->attributes = $_POST['Client'];
            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('Client %s created', $model->name));
                $this->redirect($this->createUrl('//clients'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Client'])) {
            $model->attributes = $_POST['Client'];

            if ($model->save()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('Client %s updated', $model->name));
                $this->redirect($this->createUrl('//clients'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        if ($this->getRequest()->isPostRequest) {
            if ($model->delete()) {
                $this->setFlash(self::FLASH_SUCCESS, sprintf('Client %s deleted', $model->name));
            }

            $this->redirect($this->createUrl('//clients'));
        } else {
            $this->render('delete', array(
                'model' => $model,
            ));
        }
    }

     /**
     * @param integer $id
     * @return Client
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Client::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }
}
